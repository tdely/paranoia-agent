// paranoia-agent project main.go
package main

import (
	pa "paranoia-agent/internal"

	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/otiai10/ternary"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	var err error
	f := flag.NewFlagSet("flag", flag.ExitOnError)
	var (
		cfgPath = f.String("cfg", "paranoia.yml", "configuration file")
		level   = f.String("level", "", "set log level")
		pretty  = f.Bool("pretty", false, "enable pretty console logging")
	)
	if err = f.Parse(os.Args[1:]); err != nil {
		panic(err.Error())
	}

	err = pa.LoadConfiguration(*cfgPath)
	if err != nil {
		log.Fatal().Msg(fmt.Sprintf("failed to load configuration: %s", err))
	}
	cfg := &pa.Configuration

	// Logging
	l, err := zerolog.ParseLevel(
		ternary.If(*level != "").String(*level, cfg.LogLevel))
	if err != nil {
		log.Fatal().Msg(fmt.Sprintf("failed to parse log level: %s", err))
	}
	zerolog.SetGlobalLevel(l)
	if l == zerolog.DebugLevel {
		log.Logger = log.With().Caller().Logger()
	}
	if *pretty {
		log.Logger = log.Output(
			zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339})
	}

	// Agent
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	a := pa.NewAgent(cfg.Concurrency)
	go func() {
		if err := a.Start(); err != nil {
			log.Fatal().Msg(fmt.Sprintf("error in agent: %s\n", err))
		}
	}()
	log.Info().Msg("agent started")

	<-done
	log.Info().Msg("agent stopping")
	if err := a.Shutdown(); err != nil {
		log.Fatal().Msg(fmt.Sprintf("agent shutdown failed:%+v\n", err))
	}
	log.Info().Msg("agent stopped")
}
