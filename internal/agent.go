package internal

import (
	pa "paranoia-agent/pkg"
	pprb "paranoia-agent/plugins/probes"
	prpt "paranoia-agent/plugins/reporters"

	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v3"
)

// Config provides the root container for application configuration.
type Config struct {
	// Minimum level of messages to log.
	//
	// Available values are:
	//
	//    - "info"
	//    - "warn"
	//    - "error"
	//    - "fatal"
	//    - "panic"
	//    - ""
	LogLevel string `yaml:"log_level"`

	// Number of probes to run in parallel.
	Concurrency int `yaml:"concurrency"`

	// Interval at which to probe.
	Interval int `yaml:"interval"`

	// Directory with Probe plugins.
	ProbesDir string `yaml:"probes_dir"`

	// Directory with Reporter plugins.
	ReportersDir string `yaml:"reporters_dir"`

	// Settings for probes.
	Probes map[string]yaml.Node `yaml:"probes"`

	// Settings for reporters.
	Reporters map[string]yaml.Node `yaml:"reporters"`
}

const modExt string = ".so"
const defLogLevel string = "info"
const defInterval int = 30

// Configuration container for the application.
var Configuration Config

// LoadConfiguration reads a YAML file into Configuration.
func LoadConfiguration(path string) error {
	Configuration = Config{
		LogLevel:    defLogLevel,
		Concurrency: runtime.GOMAXPROCS(0),
		Interval:    defInterval,
	}

	if path != "" {
		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()
		err = yaml.NewDecoder(file).Decode(&Configuration)
		if err != nil {
			return err
		}
	}
	return nil
}

// The Agent handles the control of probes and the result reporting.
type Agent struct {
	wp          workerPool
	Concurrency int
	openCh      chan struct{}
	stopCh      chan struct{}
	wg          sync.WaitGroup
	probes      []pprb.Prober
	reporters   []prpt.Reporter
	sync.Mutex
}

// NewAgent sets up and returns a new Agent.
func NewAgent(concurrency int) *Agent {
	a := Agent{Concurrency: concurrency}
	a.openCh = make(chan struct{})
	a.stopCh = make(chan struct{})
	a.wp = workerPool{}
	return &a
}

// nolint: dupl
func loadProbes(dir string) (probes []pprb.Prober, err error) {
	files, err := filepath.Glob(filepath.Join(dir, fmt.Sprintf("*%s", modExt)))
	if err != nil {
		return
	}
	for _, f := range files {
		var newFunc func(string, *yaml.Node) pprb.Prober
		log.Debug().Msg(fmt.Sprintf("loading probe plugin '%s'", f))
		newFunc, err = pprb.Load(f)
		if err != nil {
			return probes, fmt.Errorf("%s while loading %s", err, f)
		}
		fname := filepath.Base(f)
		pname := fname[:len(fname)-len(modExt)]
		if settings, ok := Configuration.Probes[pname]; ok {
			probes = append(probes, newFunc(pname, &settings))
		}
	}
	return
}

// nolint: dupl
func loadReporters(dir string) (reporters []prpt.Reporter, err error) {
	files, err := filepath.Glob(filepath.Join(dir, fmt.Sprintf("*%s", modExt)))
	if err != nil {
		return
	}
	for _, f := range files {
		var newFunc func(string, *yaml.Node) prpt.Reporter
		log.Debug().Msg(fmt.Sprintf("loading reporter plugin '%s'", f))
		newFunc, err = prpt.Load(f)
		if err != nil {
			return reporters, fmt.Errorf("%s while loading %s", err, f)
		}
		fname := filepath.Base(f)
		pname := fname[:len(fname)-len(modExt)]
		if settings, ok := Configuration.Reporters[pname]; ok {
			reporters = append(reporters, newFunc(pname, &settings))
		}
	}
	return
}

// Start the periodic launch of probes.
func (a *Agent) Start() error {
	var err error

	a.probes, err = loadProbes(Configuration.ProbesDir)
	if err != nil {
		return err
	}

	a.reporters, err = loadReporters(Configuration.ReportersDir)
	if err != nil {
		return err
	}

	workCh, responsesCh := a.wp.Start()

	ticker := time.NewTicker(time.Duration(Configuration.Interval) * time.Second)
	defer ticker.Stop()
run:
	for {
		select {
		case <-a.stopCh:
			close(a.openCh)
			break run
		case <-ticker.C:
			a.run(workCh, responsesCh)
		}
	}
	a.probes = nil
	a.reporters = nil
	return nil
}

func (a *Agent) run(workCh chan<- pprb.Prober, responsesCh <-chan []pa.Response) {
	var results pa.ResponseSet
	var err error
	// a.wp.Lock()
	for i := range a.probes {
		workCh <- a.probes[i]
		a.wg.Add(1)
		go func() {
			results.Responses = append(results.Responses, <-responsesCh...)
			a.wg.Done()
		}()
	}
	a.wg.Wait()
	// a.wp.Unlock()

	for i := range a.reporters {
		a.wg.Add(1)
		reporter := a.reporters[i]
		go func() {
			err = reporter.Report(&results)
			if err != nil {
				log.Warn().Msg(err.Error())
			}
			a.wg.Done()
		}()
	}
	a.wg.Wait()
}

// Shutdown the agent gracefully, allowing any running probes to finish before
// terminating but preventing new probes from starting. Reporters will run
// before terminiation if any probes are running.
func (a *Agent) Shutdown() error {
	a.Lock()
	close(a.stopCh)

	a.wp.Stop()
	<-a.openCh

	a.stopCh = make(chan struct{})
	a.Unlock()
	return nil
}
