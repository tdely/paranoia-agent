package internal

import (
	"fmt"
	"sync"

	"github.com/rs/zerolog/log"

	pa "paranoia-agent/pkg"
	pprb "paranoia-agent/plugins/probes"
)

type workerPool struct {
	sync.Mutex

	workers []worker

	workCh      chan pprb.Prober
	responsesCh chan []pa.Response
}

type worker struct {
	id          int
	workCh      chan pprb.Prober
	responsesCh chan []pa.Response
}

func (w *worker) start() {
	go func() {
		log.Info().Msg(fmt.Sprintf("worker%d: starting", w.id))
		for {
			prober := <-w.workCh
			if prober == nil {
				log.Info().Msg(fmt.Sprintf("worker%d: stopping", w.id))
				break
			}
			log.Debug().Msg(fmt.Sprintf("worker%d: received %s work", w.id, prober.Name()))
			w.responsesCh <- prober.Probe()
		}
	}()
}

// Start the workerPool.
func (wp *workerPool) Start() (workCh chan<- pprb.Prober, respCh <-chan []pa.Response) {
	log.Info().Msg("starting workerpool")
	if wp.workCh != nil {
		log.Panic().Msg("workerPool already started")
	}
	wp.workCh = make(chan pprb.Prober)
	wp.responsesCh = make(chan []pa.Response)

	for i := int(0); i < Configuration.Concurrency; i++ {
		w := worker{
			id:          i + 1,
			workCh:      wp.workCh,
			responsesCh: wp.responsesCh,
		}
		wp.workers = append(wp.workers, w)
	}
	for i := range wp.workers {
		wp.workers[i].start()
	}

	return wp.workCh, wp.responsesCh
}

// Stop the workerPool.
func (wp *workerPool) Stop() {
	log.Info().Msg("stopping workerpool")
	if wp.workCh == nil {
		log.Panic().Msg("workerPool wasn't started")
	}
	wp.Lock()
	close(wp.workCh)
	wp.workCh = nil
	wp.Unlock()
}
