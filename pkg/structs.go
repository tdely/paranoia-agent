package pkg

import (
	"sync"
)

// ProbeResult represents the outcome of a probe.
type ProbeResult int

// Possible probe results.
const (
	OK = iota
	Noop
	Remedied
	Warning
)

// Response provides the container for probe result and operation data.
type Response struct {
	// Probe identifier
	ProbeID string
	// Target of the probe
	Target string
	// Probe outcome
	Result ProbeResult
	// Type of metric
	Type string
	// Metric data
	Metrics map[string]interface{}
	// Operation information.
	Message string
}

// ResponseSet to be returned by a probe.
type ResponseSet struct {
	sync.Mutex
	Responses []Response
}
