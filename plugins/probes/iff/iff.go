// nolint:deadcode,unused
package main

import (
	pa "paranoia-agent/pkg"
	probes "paranoia-agent/plugins/probes"

	"fmt"
	"net"
	"os"
	"strings"
	"syscall"
	"unsafe"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v3"
)

type prober struct {
	name     string
	settings config
	log      zerolog.Logger
}

type config struct {
	Ifaces []string `yaml:"ifaces"`
}

// NewProber returns a new prober configured with settings.
func NewProber(name string, settings *yaml.Node) probes.Prober {
	p := prober{
		name: name,
		log:  log.With().Str("iff", name).Logger(),
	}

	if err := settings.Decode(&p.settings); err != nil {
		p.log.Panic().Msg(err.Error())
	}

	return &p
}

func (p *prober) Name() string {
	return p.name
}

// Probe network interface flags for UP and RUNNING states.
func (p *prober) Probe() []pa.Response {
	var results []pa.Response

	p.log.Debug().Msg(fmt.Sprintf("probing: %s", p.name))

	for _, i := range p.settings.Ifaces {
		response := p.probeInterface(i)
		results = append(results, response)
	}
	return results
}

func (p *prober) probeInterface(ifaceName string) pa.Response {
	response := pa.Response{
		ProbeID: p.name,
		Target:  ifaceName,
		Metrics: make(map[string]interface{}),
	}
	iface, err := net.InterfaceByName(ifaceName)
	if err != nil {
		response.Result = pa.Noop
		response.Message = err.Error()
		return response
	}

	// DeviceUp (IFF_UP) is available in flags
	if !strings.Contains(iface.Flags.String(), "up") {
		response.Metrics["up"] = 0
	} else {
		response.Metrics["up"] = 1
	}
	// LinkUp (IFF_RUNNING) must be retrieved from syscall
	response.Metrics["running"], err = getRunningState(iface)
	if err != nil {
		response.Message = err.Error()
	}

	if response.Metrics["up"] == 0 || response.Metrics["running"] == 0 {
		response.Result = pa.Warning
	}
	return response
}

func getRunningState(i *net.Interface) (int, error) {
	state := 0
	tab, err := syscall.NetlinkRIB(syscall.RTM_GETLINK, syscall.AF_UNSPEC)
	if err != nil {
		return state, os.NewSyscallError("netlinkrib", err)
	}
	msgs, err := syscall.ParseNetlinkMessage(tab)
	if err != nil {
		return state, os.NewSyscallError("parsenetlinkmessage", err)
	}
	var running bool
	for _, m := range msgs {
		if m.Header.Type == syscall.RTM_NEWLINK {
			ifim := (*syscall.IfInfomsg)(unsafe.Pointer(&m.Data[0]))
			if ifim.Index == int32(i.Index) {
				running = ((ifim.Flags & syscall.IFF_RUNNING) != 0)
			}
		}
	}
	if running {
		state = 1
	}
	return state, nil
}
