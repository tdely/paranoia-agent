package probes

import (
	pa "paranoia-agent/pkg"

	"fmt"
	"plugin"

	"gopkg.in/yaml.v3"
)

// Prober is the interface that probe plugins must implement.
type Prober interface {
	// Run the probe.
	Probe() []pa.Response
	// Get probe name.
	Name() string
}

func getNewProber(p *plugin.Plugin) (func(string, *yaml.Node) Prober, error) {
	var attr string = "NewProber"
	sym, err := p.Lookup(attr)
	if err != nil {
		return nil, err
	}
	newFunc, ok := sym.(func(string, *yaml.Node) Prober)
	if !ok {
		return nil, fmt.Errorf("unexpected type from plugin symbol '%s'", attr)
	}
	return newFunc, nil
}

// Load prober plugin module returning a function for spawning a new Prober
// instance.
func Load(f string) (func(string, *yaml.Node) Prober, error) {
	var err error
	var p *plugin.Plugin
	var newFunc func(string, *yaml.Node) Prober

	p, err = plugin.Open(f)
	if err != nil {
		return nil, err
	}
	newFunc, err = getNewProber(p)
	if err != nil {
		return nil, fmt.Errorf("%s while loading %s", err, f)
	}
	return newFunc, nil
}
