// nolint:deadcode,unused
package main

import (
	pa "paranoia-agent/pkg"
	reporters "paranoia-agent/plugins/reporters"

	"fmt"

	influx "github.com/influxdata/influxdb1-client/v2"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v3"
)

type reporter struct {
	settings config
	client   influx.Client
	name     string
	log      zerolog.Logger
}

type config struct {
	Tags       map[string]string `yaml:"tags"`
	Database   string            `yaml:"database"`
	HTTPConfig influx.HTTPConfig `yaml:"http"`
}

// NewReporter returns a new reporter configured with settings.
func NewReporter(name string, settings *yaml.Node) reporters.Reporter {
	var err error
	r := reporter{
		name: name,
		log:  log.With().Str("reporter", name).Logger(),
	}

	if err = settings.Decode(&r.settings); err != nil {
		r.log.Panic().Msg(err.Error())
	}
	r.client, err = influx.NewHTTPClient(r.settings.HTTPConfig)
	if err != nil {
		r.log.Panic().Msg(err.Error())
	}

	return &r
}

func (r *reporter) Report(set *pa.ResponseSet) error {
	bp, _ := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database: r.settings.Database,
	})

	for _, resp := range set.Responses {
		if len(resp.Metrics) == 0 {
			r.log.Info().
				Msg(fmt.Sprintf("skipping metricless result: %s", resp.ProbeID))
			continue
		}
		pt, err := influx.NewPoint(resp.ProbeID, r.settings.Tags, resp.Metrics)
		if err != nil {
			r.log.Warn().Msg(err.Error())
			continue
		}
		bp.AddPoint(pt)
	}

	return r.client.Write(bp)
}
