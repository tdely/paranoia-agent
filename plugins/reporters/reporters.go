package reporters

import (
	pa "paranoia-agent/pkg"

	"fmt"
	"plugin"

	"gopkg.in/yaml.v3"
)

// Reporter is the interface that reporter plugins must implement.
type Reporter interface {
	// Run the reporter.
	Report(*pa.ResponseSet) error
}

func getNewReporter(p *plugin.Plugin) (func(string, *yaml.Node) Reporter, error) {
	var attr string = "NewReporter"
	sym, err := p.Lookup(attr)
	if err != nil {
		return nil, err
	}
	newFunc, ok := sym.(func(string, *yaml.Node) Reporter)
	if !ok {
		return nil, fmt.Errorf("unexpected type from plugin symbol '%s'", attr)
	}
	return newFunc, nil
}

// Load reporter plugin module returning a function for spawning a new Reporter
// instance.
func Load(f string) (func(string, *yaml.Node) Reporter, error) {
	var err error
	var p *plugin.Plugin
	var newFunc func(string, *yaml.Node) Reporter

	p, err = plugin.Open(f)
	if err != nil {
		return nil, err
	}
	newFunc, err = getNewReporter(p)
	if err != nil {
		return nil, fmt.Errorf("%s while loading %s", err, f)
	}
	return newFunc, nil
}
