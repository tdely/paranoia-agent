// nolint:deadcode,unused
package main

import (
	pa "paranoia-agent/pkg"
	reporters "paranoia-agent/plugins/reporters"

	"fmt"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v3"
)

type reporter struct {
	settings config
	name     string
	log      zerolog.Logger
}

type config struct{}

// NewReporter returns a new reporter configured with settings.
func NewReporter(name string, settings *yaml.Node) reporters.Reporter {
	var err error
	r := reporter{
		name: name,
		log:  log.With().Str("reporter", name).Logger(),
	}

	if err = settings.Decode(&r.settings); err != nil {
		r.log.Panic().Msg(err.Error())
	}
	return &r
}

// Report probe responses to stdout.
func (r *reporter) Report(set *pa.ResponseSet) error {
	for _, resp := range set.Responses {
		fmt.Printf("ProbeID: %s\n", resp.ProbeID)
		fmt.Printf("Target:  %s\n", resp.Target)
		for k, v := range resp.Metrics {
			fmt.Printf("+ %s: %v\n", k, v)
		}
		fmt.Printf("Result:  %d\n", resp.Result)
		if resp.Message != "" {
			fmt.Printf("Message: %v\n", resp.Message)
		}
		fmt.Println("")
	}

	return nil
}
